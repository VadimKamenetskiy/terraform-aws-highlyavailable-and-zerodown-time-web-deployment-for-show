#!/bin/bash
sudo yum -y update
sudo yum -y install httpd
sudo yum -y install git

cd /var/www/html
git clone https://gitlab.com/VadimKamenetskiy/example_html.git .

sudo service httpd start
sudo chkconfig httpd on
